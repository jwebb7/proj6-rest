<html>
    <head>
        <title>CIS 322 REST</title>
    </head>

    <body>
        <h1>ACP Database Display Options</h1>

    <form action="" method="POST">
        <label for="display">Open or Close</label>
        <select name="display" id="display">
          <option value="placeholder">Select</option>
          <option value="listAll">Open and Close</option>
          <option value="listOpenOnly">Open</option>
          <option value="listCloseOnly">Close</option>
        </select>

        <label for="format">JSON or CSV</label>
        <select name="format" id="format">
          <option value="placeholder">Select</option>
          <option value="csv">CSV</option>
          <option value="json">JSON</option>
        </select>

        <label for="top">Top</label>
        <input type="number" name="top" id="top" value="0" />
        <input type="submit" name="button" value="Display"/>
    </form>
        <p></p>

        <?php
        $display_list = ["listAll","listOpenOnly","listCloseOnly"];
        $format_list = ["json", "csv"];
        $display = $_POST["display"];
        $darray = array("listAll"=> "Open and Close", "listOpenOnly"=> "Open", "listCloseOnly"=> "Close");
        $dval = $darray[$display];
        $format = $_POST["format"];
        $top = $_POST["top"];

        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 2; $j++) {
                if ($display_list[$i] == $display && $format_list[$j] == $format) {
                    echo "<h2>$dval times as $format:</h2>";
                    $url = "http://laptop-service:5000/".$display."/".$format."?top=".$top;
                    $json = file_get_contents($url);
                    $obj = json_decode($json);
                    if ($display == "listAll") {
                        $open = $obj->open;
                        $close = $obj->close;
                        echo "OPEN:\n";
                        foreach($open as $l) {
                            echo "<li>$l</li>";
                        }
                        echo "CLOSE:\n";
                        foreach($close as $l) {
                            echo "<li>$l</li>";
                        }
                    }
                    if ($display == "listOpenOnly") {
                        $open = $obj->open;
                        echo "OPEN:\n";
                        foreach($open as $l) {
                            echo "<li>$l</li>";
                        }
                    }
                    if ($display == "listCloseOnly") {
                        $close = $obj->close;
                        echo "CLOSE:\n";
                        foreach($close as $l) {
                            echo "<li>$l</li>";
                        }
                    }
              }
            }
        }
        ?>
    </body>
</html>
